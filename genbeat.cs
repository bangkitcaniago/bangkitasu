﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class genbeat : MonoBehaviour
{
    public Transform beat;
    private float[] beatsSec = { 1, 4, 7, 12, 15, 25 };
    void Update()
    {
        StartCoroutine(EverySec(1, 2));
    }

    private void Start()
    {
    }

    IEnumerator EverySec(float time, float y)
    {
        yield return new WaitForSeconds(time);
        genbeats(y);

        // Debug.Log("Entoooot2");
    }

    Object genbeats(float randomY)
    {
        return Instantiate(beat, new Vector3(-15, randomY, 0), Quaternion.identity);
    }
}
